import request from 'request';

export default class ApiService {
  constructor() { }
  async post(formData) {
    const url = process.env.SERVICE
    const promise = await new Promise((resolve, reject) => {
      request.post({ url, formData: formData }, (err, httpResponse, body) => {
        if (err) {
          reject(err)
        }
        resolve(body)
      });
    })
    return promise;
  };
}