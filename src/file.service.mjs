import fs from 'fs';
export default class FileService {
  constructor( {pathIn, pathOut, extension}) {
    this.pathIn = pathIn.endsWith('/') ? pathIn : pathIn + '/'
    this.pathOut = pathOut.endsWith('/') ? pathOut : pathOut + '/'
    this.extension = extension;
    this.outputExtension = `done.${this.extension}`
  }

  async list() {
    return new Promise((resolve, rejects) => {
      fs.readdir(this.pathIn, (err, filenames) => {
        if (err) {
          rejects(err)
        }
        resolve(filenames.filter(filename => filename.endsWith(this.extension)))})
    })
  }

  async save(filename, data) {
    filename = filename.replace(this.extension, this.outputExtension);
    return new Promise((resolve, reject) => {
      fs.writeFile( this.pathOut+filename, data, function (err) {
        if (err) {
          reject(err);
        }
        resolve('Saved!');
      });
    })
  }

  
}