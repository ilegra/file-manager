import ApiService from './api.service'
import FileService from './file.service'
import fs from 'fs';

export default class Process {
  constructor({ pathIn, pathOut, extension }) {
    console.log( `pathIn: ${pathIn}`)
    console.log( `pathOut: ${pathOut}`)
    this.apiService = new ApiService();
    this.fileService = new FileService( {pathIn, pathOut, extension });
  }

  async init() {
    const files = await this.fileService.list();
    for (const filename of files) {
      await this.fileService.save(filename, await this.process(filename) );
    }
  }

  async process(filename) {
     const formData = {
      file: fs.createReadStream(this.fileService.pathIn + filename),
    }
    const dados = await this.apiService.post(formData);
    return dados;
  }
}