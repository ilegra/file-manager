FROM node:8

COPY . /app
RUN node --version
RUN cd /app && npm install
RUN echo "cd /app && npm start" >> init.sh
CMD ["/bin/bash", "init.sh"]
