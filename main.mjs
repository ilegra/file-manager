import Process from './src/process';
import dotenv from 'dotenv';
dotenv.load();

(async () => {
  const base = process.env.HOMEPATH ? process.env.HOMEPATH + "/data" : "";
  console.log( 'service', process.env.SERVICE)
  console.log( 'home', process.env.HOMEPATH)

  const fileSystem = new Process({
    pathIn: `${base}/in`,
    pathOut: `${base}/out`,
    extension: 'dat',
  })
  setInterval( async () => {
    await fileSystem.init()
  }, 1000)
})()